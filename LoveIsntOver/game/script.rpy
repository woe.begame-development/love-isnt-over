﻿# The game starts here.
label start:
    # This jumps to the label "chapter_example", which is kept in another file!
    jump chapter_example

    # This ends the game.
    return

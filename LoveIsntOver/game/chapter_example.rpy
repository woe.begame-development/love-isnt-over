﻿label chapter_example:
    "This is an example chapter, for showing how the custom menu and time travel variables work"

    # Default menu, don't use! It won't keep track of player choices
    # For comparison with the custom menu below
    menu:
        "This is a regular Ren'Py menu, for syntax comparison"

        "first option":
            "(regular menu) You chose the first option"

        "middle option":
            "(regular menu) Second option"

        "timetravel option" if "end" in persistent.timeline_events:
            "(regular menu) Secret third option. you have time traveled!"

        "last option":
            "(regular menu) The last option"


    # Custom Menu, use this!
    # You can either use the python function or start each line with the $ symbol
    python:
        # First create the menu with the CustomMenu class.
        # The first (mandatory) argument is the menu name, used for recording the choice made
        # The second argument (optional) Is what text is said in the menu,
        # and a third optional argument can be used to define who says it
        choose = CustomMenu("first", "This is a custom menu, which records your choices even if you time travel")

        # Add each option
        # The first argument is the text displayed on the button
        # The second argument is which label the choice should take you to
        # The third (optional) keyword argument "choice_name" defines what name will be recorded in persistent.choices
        # If not defined choice_name == text
        # Last optional keyword argument is a condition. If false, the option is hidden
        choose.option("first option", "choice1", "1")
        choose.option("middle option", "choice2", "2")
        choose.option("timetravel option", "choice3", "3", "end" in persistent.timeline_events)
        choose.option("last option", "choice4", "4")

        # Finally, display the menu
        choose.display()

label dontreachthisone:
    "(custom menu) If you see this the menu broke"
    "The custom menu choices all take you to a specified label. If something broke (without crashing the game) the script would reach this as it is directly below the custom menu in the code"
    jump end

label choice1:
    "(custom menu) You chose the first option"
    jump end

label choice2:
    "(custom menu) Second option"
    jump end

label choice3:
    "(custom menu) Secret third option. you have time traveled!"
    jump end

label choice4:
    "(custom menu) The last option"
    jump end

label end:
    # Mark that we have reached the end in the timeline
    $ persistent.timeline_events.add("end")
    "End of timeline reached. If you load an earlier save or scroll back, or replay the game, it counts as time travel!"

    # Show the choices made
    "Choices:"
    python:
        for choice in persistent.choices:
            chosen_options = persistent.choices[choice]
            renpy.say(None, "[choice]: [chosen_options]")

    "Corrections: [persistent.corrections]"

## Custom menu class for tracking time travel

## Ren'Py doesn't allow docstrings (long comments that start and end with """)
## but I found out too late so they're still in there but commented out

init python:

    class CustomMenu():
        def __init__(self, name, text=None, speaker=None):
#         """A custom menu
#
#         Args:
#             name (str): Menu name. Used for recording the choice made in persistent.choices
#
#         Keyword Args:
#             text (str/None): Optional text displayed with the menu
#             speaker (str/None): Optional speaker for the optional text
#         """
            self.name = name
            self.text = text
            self.speaker = speaker
            self.options = []

        def option(self, text, label, option_name=None, condition=True):
#         """Add an option to the menu
#
#         Args:
#             text (str): The text displayed on the button for the option
#             label (str): The label you jump to after picking this option
#
#         Keyword Args:
#             option_name (str/None): Optional name of option for recording in persistent.choices
#             condition (bool): Condition for showing the option. Default True
#         """
            if option_name is None:
                option_name = text
            self.options.append({"name": option_name, "text": text, "label": label, "condition": condition})

        def display(self):
#         """Display the menu"""
            if self.text:
                renpy.say(self.speaker, self.text, interact=False)
            renpy.call_screen("custom_choice", self.name, self.options)

    def make_choice(choice_name, option_name):
#         """Records the choice in persistent and checks if it is a correction
#
#         Args:
#             choice_name (str): The name of the choice, as defined by the menu name
#             option_name (str): The option chosen
#         """
        choice = persistent.choices.get(choice_name, [])
        if choice_name in persistent.choices:
            ## If it isn't identical to the most recent choice it is a "correction"
            if not option_name == choice[-1]:
                persistent.corrections += 1

        choice.append(option_name)

        persistent.choices[choice_name] = choice


## Subjectively this could also be in screens.rpy,
## but I prefer to keep it with the custom menuclass code since they are connected

screen custom_choice(menu_name, options):
    ## Tell the custom choice to use the same style as the normal choice :)
    style_prefix "choice"

    vbox:
        for option in options:
            if not option["condition"]:
                continue
            textbutton option["text"]:
                ## When clicked, do function make_choice, with the arguments menu_name and option["name"],
                ## also jump to label option["label"]
                action [Function(make_choice, menu_name, option["name"]), Jump(option["label"])]


